#!/usr/bin/env python
# -*- coding: utf8 -*-

# Author: Henrique Fernandes
import argparse
from pyftpServer import main

parser = argparse.ArgumentParser(
	description=__doc__,
	formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-user', action='append', type=str, default=[], help='Usuario.')
parser.add_argument('-pass', action='append', type=str, default=[], help='Senha.')
parser.add_argument('-port', type=int, default=[], help='Porta.')
parser.add_argument('-path', action='append', type=str, help='Path')

args = parser.parse_args()

main(args.usuario, args.senha, args.path, args.porta)
