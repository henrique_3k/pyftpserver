#!/usr/bin/env python

from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import ThreadedFTPServer


def main(usuario, senha, path, porta):
    authorizer = DummyAuthorizer()
    authorizer.add_user(str(usuario[0]), str(senha[0]), path[0], perm='elradfmwM')
    handler = FTPHandler
    handler.authorizer = authorizer
    server = ThreadedFTPServer(('', porta), handler)
    server.serve_forever()
