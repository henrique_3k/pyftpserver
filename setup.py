#!/usr/bin/env python
# -*- coding: utf8 -*-

# Author: Henrique Fernandes


from setuptools import setup, find_packages


def read(path):
	with open(path, 'r') as file:
		return file.read()


def main():
	setup(
		name='PyFTPServer',
		version=read('VERSION'),
		author='Henrique Fernandes',
		author_email='3khenrique@gmail.com',
		description='PyFTPServer',
		long_description=read('README.md'),
		packages=find_packages(),
		package_data={'': 'pyftpdlib/*'},
		
		classifiers=[
			"Development Status :: 4 - Beta",
			"Environment :: Console",
			"Programming Language :: Python",
		],
		entry_points={
			'setuptools.installation': [
				'eggsecutable = my_package.some_module:main_func',
			]
		},
		data_files=[('', ['__main__.py', 'pyftpServer.py', ])]
	)


if __name__ == '__main__':
	main()
