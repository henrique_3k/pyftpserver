# README #

PyFtpServer

* A simple ftp server made in python

### Dependencies ###

* [Python3.6](https://www.python.org/downloads/release/python-361/)
* [pyftpdlib](https://pypi.python.org/pypi/pyftpdlib/)
* Or via pip: pip install pyftpdlib

### How to Use? ###

* Parameters: -user ['user'] -pass ['pass'] -port [port]  -path ['path']
* in terminal, execute: Python __main__.py Parameters

### Contribution guidelines ###

* Writing tests
* Code review
* Create a server with multiple users
* Other guidelines